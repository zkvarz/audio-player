package com.core.providers;

public enum Font {
    Raleway("fonts/raleway/Raleway-Regular.ttf");

    private final String assetPath;

    Font(String path) {
        this.assetPath = path;
    }

    public String getAssetPath() {
        return assetPath;
    }
}
