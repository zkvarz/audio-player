package com.core.models;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.core.structure.models.model.IModel;


public class BitmapModel implements IModel {
    private final Bitmap bitmap;

    public BitmapModel(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.bitmap, 0);
    }

    private BitmapModel(Parcel in) {
        this.bitmap = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Parcelable.Creator<BitmapModel> CREATOR = new Parcelable.Creator<BitmapModel>() {
        public BitmapModel createFromParcel(Parcel source) {
            return new BitmapModel(source);
        }

        public BitmapModel[] newArray(int size) {
            return new BitmapModel[size];
        }
    };
}
