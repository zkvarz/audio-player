package com.core.images.cache.memory;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.core.images.cache.ImagesCache;

public class MemoryImagesCache  implements ImagesCache{
	private static MemoryImagesCache cache;

    private final LruCache<String, Bitmap> mMemoryCache;

	private MemoryImagesCache() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 8;
		mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
			@Override
			protected int sizeOf(String key, Bitmap bitmap) {
				// The cache size will be measured in kilobytes rather than
				// number of items.
				return bitmap.getRowBytes() * bitmap.getHeight() / 1024;
			}
		};
	}

	public static synchronized MemoryImagesCache get() {
		if (cache == null) {
			cache = new MemoryImagesCache();
		}
		return cache;
	}

    @Override
	public void addToCache(String key, Bitmap bitmap) {
		if (getFromCache(key) == null) {
			mMemoryCache.put(key, bitmap);
		}
	}

    @Override
	public Bitmap getFromCache(String key) {
		return mMemoryCache.get(key);
	}

}
