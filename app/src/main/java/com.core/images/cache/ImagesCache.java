package com.core.images.cache;

import android.graphics.Bitmap;


public interface ImagesCache {

    public void addToCache(String key, Bitmap bitmap);

    public Bitmap getFromCache(String key);
}
