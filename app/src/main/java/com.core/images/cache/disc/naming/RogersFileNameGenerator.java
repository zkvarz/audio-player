package com.core.images.cache.disc.naming;

public class RogersFileNameGenerator implements FileNameGenerator {

    @Override
	public String generate(String imageUri) {
        int index = imageUri.lastIndexOf("/") + 1;
        imageUri = imageUri.substring(index);
        imageUri = imageUri.replace(" ", "_");

        int extensionIndex = imageUri.lastIndexOf(".") + 1;
        String extension = imageUri.substring(extensionIndex);

        imageUri = imageUri.substring(0, extensionIndex);

        String result = imageUri.replaceAll("[^\\p{L}\\p{N}]", "");


        int FILE_NAME_MAX_LENGTH = 64;
        if(result.length()> FILE_NAME_MAX_LENGTH){
            result=result.substring(0, FILE_NAME_MAX_LENGTH);
        }

        if (extensionIndex != -1) {
            result = result + "." + extension;
        }
        return result;
	}
}
