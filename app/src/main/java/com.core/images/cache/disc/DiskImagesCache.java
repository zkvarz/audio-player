package com.core.images.cache.disc;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.core.images.cache.ImagesCache;
import com.core.images.cache.disc.impl.UnlimitedDiskCache;
import com.core.images.cache.disc.naming.Md5FileNameGenerator;
import com.core.helpers.FileHelper;
import com.core.structure.main.CoreApplication;

import java.io.File;
import java.io.IOException;

public class DiskImagesCache extends UnlimitedDiskCache implements ImagesCache {
    private static DiskImagesCache cache;

    public static DiskImagesCache get() {
        if (cache == null) {
            cache = new DiskImagesCache();
        }
        return cache;
    }


    private static final String IMAGES_FOLDER = "images";

    private DiskImagesCache() {
        super(FileHelper.createTempFolder(CoreApplication.getApplication(), IMAGES_FOLDER), null, new Md5FileNameGenerator());
    }

    @Override
    public void addToCache(String key, Bitmap bitmap) {
        try {
            save(key, bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Bitmap getFromCache(String key) {
        File cachedFile = get(key);
        return BitmapFactory.decodeFile(cachedFile.getAbsolutePath());
    }


}
