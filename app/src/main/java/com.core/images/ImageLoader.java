package com.core.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.core.helpers.BitmapHelper;
import com.core.helpers.ContentUtils;
import com.core.helpers.DeviceHelper;
import com.core.images.cache.ImagesCache;
import com.core.images.cache.disc.DiskImagesCache;
import com.core.models.BitmapModel;
import com.core.structure.models.result.Result;
import com.core.structure.requests.abs.client.GetRequest;
import com.core.structure.requests.mock.ICallback;
import com.core.structure.requests.models.RemoteResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 *  Loads images from urls and local uris and cache they into cache
 */
public class ImageLoader extends GetRequest<Result<BitmapModel>> {
    private final ImagesCache mImagesCache = DiskImagesCache.get();

    private final int mImageWidth;
    private final int mImageHeight;
    private final String mUrl;

    public ImageLoader(ICallback<Result<BitmapModel>> callback, String url,
                       int desiredWidth, int desiredHeight) {
        super(callback);
        mImageWidth = desiredWidth;
        mImageHeight = desiredHeight;
        this.mUrl = url;
    }

    public ImageLoader(Context context, ICallback<Result<BitmapModel>> callback, String url) {
        this(callback, url, DeviceHelper.getDisplayWidth(context), DeviceHelper.getDisplayHeight(context));
    }

    @Override
    public void doWork() {
        Bitmap bitmap = mImagesCache.getFromCache(mUrl);
        if (bitmap == null) {
            if (isInternalFile()) {
                try {
                    processStream(new FileInputStream(new File(mUrl)));
                } catch (Exception e) {
                    onError(e);
                }
            } else {
                super.doWork();
            }
        } else {
            onSuccess(new Result<>(new BitmapModel(bitmap)));
        }
    }

    @Override
    protected void parseResponse(RemoteResponse response) {
        if (response.isSuccess()) {
            processStream(response.getInputStream());
        }
    }

    private void processStream(InputStream stream) {
        try {
            Bitmap bitmap = BitmapHelper.getResizedFromInputStream(stream,
                    mImageWidth, mImageHeight);
            success(bitmap);
        } catch (Exception e) {
            onError(e);
        } finally {
            try {
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void success(Bitmap bitmap) {
        Bitmap result = bitmap;
        if (isInternalFile()) {
            result = BitmapHelper.rotate(bitmap, ContentUtils.getRotation(mUrl));
        }
        mImagesCache.addToCache(mUrl, result);
        onSuccess(new Result<>(new BitmapModel(bitmap)));
    }

    @Override
    protected String buildUrl() {
        return mUrl;
    }

    public String getUrl() {
        return mUrl;
    }

    private boolean isInternalFile() {
        boolean result = false;
        if (!TextUtils.isEmpty(mUrl) && !mUrl.startsWith("http")) {
            result = true;
        }
        return result;
    }
}
