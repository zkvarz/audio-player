package com.core.images;

import android.net.Uri;

import com.core.structure.main.CoreApplication;
import com.core.structure.requests.abs.Request;
import com.core.structure.requests.mock.ICallback;

import java.io.File;

import static com.core.helpers.ContentUtils.copyFromLocalUri;

public class SaveImageRequest extends Request<Boolean> {

    private final Uri uri;
    private final File tofile;

    public SaveImageRequest(Uri imageUri, File tofile, ICallback<Boolean> callback) {
        super(callback);
        this.tofile = tofile;
        uri = imageUri;
    }

    @Override
    public void doWork() {
        onSuccess(copyFromLocalUri(CoreApplication.getApplication(), uri, tofile));
    }
}
