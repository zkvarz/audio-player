package com.core.helpers;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.core.structure.helpers.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public class FileHelper {

    public static File getCacheDir(Context context) {
        return context.getExternalCacheDir();
    }

    public static File createCacheFile(Context context, String name) {
        return createFileIfNotExist(getCacheDir(context), name);
    }

    public static File createExternalFile(Context context, String name) {
        return createFileIfNotExist(new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath()), name);
    }

    public static File createTempFolder(Context context, String folderName) {
        File file = new File(getCacheDir(context), folderName);
        if (!file.exists() || !file.isDirectory()) {
            file.mkdirs();
        }
        return file;
    }

    public static String createUniqueFileName(Context context) {
        return DeviceHelper.getDeviceId(context) + "_" + System.currentTimeMillis();
    }


    private static File createFileIfNotExist(File dir, String name) {
        return createFile(new File(dir, name));
    }

    private static File createFile(File file) {
        if (!file.exists()) {
            try {
                if (file.createNewFile()) {
                    Log.w("FileUtils", "you can't override directory with file");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static void close(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getFileName(String path) {
        String name = path.substring(path.lastIndexOf("/") + 1);
        Logger.debug(FileHelper.class, name);
        return name;
    }


}
