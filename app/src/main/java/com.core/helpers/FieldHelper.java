package com.core.helpers;

import android.util.Patterns;

import static android.text.TextUtils.isEmpty;

public final class FieldHelper {



    public static boolean isValidPhoneNumber(CharSequence phoneNumber) {
        return !isEmpty(phoneNumber) && phoneNumber.length() >= 10 && Patterns.PHONE.matcher(phoneNumber).matches();
    }

    public static boolean isValidUrl(CharSequence target) {
        return !isEmpty(target) && Patterns.WEB_URL.matcher(target).matches();

    }

    public static boolean isValidEmail(CharSequence target) {
        return !isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static String prepareUrl(String url) {
        String prepared = url;
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            prepared = "http://" + url;
        }
        return prepared;
    }

    public static boolean isValidPassword(CharSequence s) {
        return !(isEmpty(s) || s.length() < 6);
    }

    public static String replaceEscapeSymbols(String c) {
        if (!isEmpty(c)) {
            return c.replaceAll("&amp;", "&")
                    .replaceAll("&lt;", "<")
                    .replaceAll("&gt;", ">")
                    .replaceAll("&quot;", "\"")
                    .replaceAll("&apos;", "\'");
        }
        return c;
    }
}
