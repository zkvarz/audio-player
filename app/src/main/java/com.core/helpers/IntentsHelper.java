package com.core.helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Telephony;

import java.io.File;

public class IntentsHelper {

    public static final String MARKET_WEB_LINK = "http://play.google.com/store/apps/details?id=";
    public static final String MARKET_APP_LINK = "market://details?id=";


    @SuppressLint("NewApi")
    public static void sendSMS(Activity activity, String content) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String defaultSmsPackageName = Telephony.Sms
                    .getDefaultSmsPackage(activity); // Need to change the build
            // to API 19
            Intent sendIntent = new Intent(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, content);
            // sendIntent.putExtra(Intent.EXTRA_STREAM,
            // Uri.fromFile(new File(imagePath)));
            // sendIntent.setType("image/png");
            if (defaultSmsPackageName != null) {
                sendIntent.setPackage(defaultSmsPackageName);
            }
            activity.startActivity(sendIntent);
        } else {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:"));
            sendIntent.setType("vnd.android-dir/mms-sms");
            sendIntent.putExtra("sms_body", content);
            activity.startActivity(sendIntent);
        }
    }

    public static Intent getGalleryIntent(boolean forImage) {
        Intent galleryIntent = new Intent();
        galleryIntent.setType(forImage ? "image/*" : "video/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        return Intent.createChooser(galleryIntent, "Choose");
    }

    public static Intent getCameraIntent(File file, boolean forImage) {
        Intent intent = new Intent(forImage ? MediaStore.ACTION_IMAGE_CAPTURE : MediaStore.ACTION_VIDEO_CAPTURE);
        if (forImage) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        } else {
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
        }
        return intent;
    }

    public static void openGooglePlay(Context context) {
        String packageName = context.getPackageName();
        Uri uri = Uri.parse(MARKET_APP_LINK + packageName);
        Intent marketIntent = new Intent(Intent.ACTION_VIEW, uri);
        marketIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(marketIntent);
        } catch (ActivityNotFoundException e) {
            openBrowser(context, MARKET_WEB_LINK + packageName);
        }
    }

    public static void openBrowser(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


}
