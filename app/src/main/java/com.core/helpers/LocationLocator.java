package com.core.helpers;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.core.structure.main.CoreApplication;
import com.core.structure.manager.AbsManager;

@SuppressWarnings("FieldCanBeLocal")
public class LocationLocator extends AbsManager<Location> implements LocationListener {

    private static LocationLocator locationLocator;

    private LocationManager locationManager;


    /*
     * Updates are restricted to one every 10 seconds, and only when movement of
     * more than 10 meters has been detected.
     */
    private final int minTime = 10 * 1000; // minimum time interval between
    // location updates, in milliseconds
    private final int minDistance = 100; // minimum distance between location
    // updates, in meters
    private Location lastKnownLocation;

    private LocationLocator() {
        // Get reference to Location Manager
        locationManager = (LocationManager) CoreApplication.getApplication()
                .getSystemService(Context.LOCATION_SERVICE);
        initLocation();
    }

    public static synchronized LocationLocator get() {
        if (locationLocator == null) {
            locationLocator = new LocationLocator();
        }
        return locationLocator;
    }

    public Location getLastKnownLocation() {
        if(lastKnownLocation==null){
            return new Location("GPS");
        }
        return lastKnownLocation;
    }


    private void initLocation() {
//        boolean isGPSEnabled = locationManager
//                .isProviderEnabled(LocationManager.GPS_PROVIDER);
//        boolean isNetworkEnabled = locationManager
//                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
//        lastKnownLocation = new Location("");
//        String provider;
//        if (isNetworkEnabled) {
//            provider = LocationManager.NETWORK_PROVIDER;
//        } else if (isGPSEnabled) {
//            provider = LocationManager.GPS_PROVIDER;
//        } else {
//            provider = locationManager.getBestProvider(new Criteria(), true);
//        }
//        if (provider != null && (isGPSEnabled || isNetworkEnabled)) {
//            locationManager.requestLocationUpdates(provider, minTime,
//                    minDistance, this);
//            lastKnownLocation = locationManager.getLastKnownLocation(provider);
//        }

    }

    @Override
    public void onLocationChanged(Location location) {
        lastKnownLocation = location;
        notifySuccess(location);
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void obtainModel() {
        initLocation();
    }
}