package com.core.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Base64;
import android.util.DisplayMetrics;

import com.core.structure.helpers.IOUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.core.helpers.FileHelper.createCacheFile;
import static com.core.helpers.FileHelper.createUniqueFileName;

public class BitmapHelper {

    public static Bitmap getResizedFromInputStream(InputStream is,
                                                   int desWidth, int desHeight) throws IOException {
        byte[] imageData = IOUtils.toByteArray(is);
        final BitmapFactory.Options options = new BitmapFactory.Options();
        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(imageData, 0, imageData.length, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options,
                desWidth, desHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        Bitmap myBitmap;
        myBitmap = BitmapFactory.decodeByteArray(imageData, 0,
                imageData.length, options);
        return myBitmap;
    }

    public static Bitmap decodeBitmapFitToWidth(Context context, String path) {
        Bitmap bitmap = null;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream inputStream = null;
        try {
            inputStream = context.getAssets().open(path);
            BitmapFactory.decodeStream(inputStream, null, options);

            int width = (int) (DeviceHelper.getDisplayWidth(context) / DeviceHelper.getDensity(context));
            int height = calculateHeight(width, calculateAspectRatio(options));

            options.inSampleSize = calculateInSampleSize(options, width, height);
            options.inJustDecodeBounds = false;
            inputStream = context.getAssets().open(path);
            bitmap = BitmapFactory.decodeStream(inputStream, null, options);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                assert inputStream != null;
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return bitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int desWidth, int desHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > desWidth || width > desHeight) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and
            // keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > desHeight
                    && (halfWidth / inSampleSize) > desWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private static float calculateAspectRatio(BitmapFactory.Options options) {
        return (float) options.outWidth / (float) options.outHeight;
    }

    private static int calculateHeight(int width, float aspect) {
        return (int) (width / aspect);
    }

    public static Bitmap rotate(Bitmap src, int degrees) {
        if (degrees == 0)
            return src;
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees); // anti-clockwise by 90 degrees
        // create a new bitmap from the original using the matrix to transform
        // the result
        Bitmap result = Bitmap.createBitmap(src, 0, 0, src.getWidth(),
                src.getHeight(), matrix, true);
        result.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        // src.recycle();
        System.gc();
        return result;
    }

    public static String cacheOnDisk(Context context, Bitmap bitmap) throws FileNotFoundException {
        File filePath = createCacheFile(context, createUniqueFileName(context) + ".png");
        FileOutputStream out = new FileOutputStream(filePath);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        // PNG is a loss less format, the compression factor (100) is ignored
        try {
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return filePath.getAbsolutePath();
    }

    public static byte[] toBytes(Bitmap bitmap) {
        ByteArrayOutputStream ByteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, ByteStream);
        return ByteStream.toByteArray();
    }

    public static String base64Encode(Bitmap bitmap) {
        return Base64.encodeToString(toBytes(bitmap), Base64.DEFAULT);
    }

    public static Bitmap toBitmap(byte[] bytes) {
        try {
            return BitmapFactory.decodeByteArray(bytes, 0,
                    bytes.length);
        } catch (Exception e) {
            e.getMessage();
            return null;
        }

    }

}
