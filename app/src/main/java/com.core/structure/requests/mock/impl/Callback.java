package com.core.structure.requests.mock.impl;


import com.core.structure.models.error.IError;
import com.core.structure.requests.mock.ICallback;

public class Callback<T> implements ICallback<T> {

    @Override
    public void onSuccess(T t) {
        onAnyResponse();
    }

    public void onAnyResponse() {

    }

    @Override
    public void onError(IError error) {
        onAnyResponse();
    }
}
