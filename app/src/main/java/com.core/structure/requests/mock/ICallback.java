package com.core.structure.requests.mock;


import com.core.structure.models.error.IError;

/**
 * The callback for  background Request
 *
 * @param <T> - type of Returned value
 */
public interface ICallback<T> {

    public void onSuccess(T t);

    public void onError(IError error);
}
