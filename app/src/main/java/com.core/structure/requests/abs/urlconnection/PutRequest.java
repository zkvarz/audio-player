package com.core.structure.requests.abs.urlconnection;

import com.core.structure.models.result.IResult;
import com.core.structure.requests.mock.ICallback;

public abstract class PutRequest<T  extends IResult> extends HttpRequest<T> {

    public PutRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpMethod getHttpMethod() {
        return HttpMethod.PUT;
    }
}
