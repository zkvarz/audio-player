package com.core.structure.requests.abs.client;

import com.core.structure.requests.abs.client.base.QueryRequest;
import com.core.structure.requests.mock.ICallback;


public abstract class GetRequest<T> extends QueryRequest<T> {



    public GetRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpRequestFactory.HttpMethod getHttpMethod() {
        return HttpRequestFactory.HttpMethod.GET;
    }

}
