package com.core.structure.requests.abs.client;

import com.core.structure.requests.abs.client.base.EntityRequest;
import com.core.structure.requests.mock.ICallback;

public abstract class PutRequest<T> extends EntityRequest<T> {

    public PutRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpRequestFactory.HttpMethod getHttpMethod() {
        return HttpRequestFactory.HttpMethod.PUT;
    }
}
