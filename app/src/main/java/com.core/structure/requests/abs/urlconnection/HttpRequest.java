package com.core.structure.requests.abs.urlconnection;

import com.core.structure.manager.comunication.ssl.ExX509TrustManager;
import com.core.structure.requests.abs.Request;
import com.core.structure.requests.mock.ICallback;
import com.core.structure.requests.models.RemoteResponse;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import static android.text.TextUtils.isEmpty;

public abstract class HttpRequest<T> extends Request<T> {

    private static final int TIMEOUT = 5000;

    public static final String CHARSET_UTF8 = "UTF-8";
    public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String ENCODING_GZIP = "gzip";
    public static final String HEADER_ACCEPT = "Accept";
    public static final String HEADER_ACCEPT_CHARSET = "Accept-Charset";
    public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_CONTENT_ENCODING = "Content-Encoding";
    public static final String HEADER_CONTENT_LENGTH = "Content-Length";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_EXPIRES = "Expires";
    private static final String BOUNDARY = "00content0boundary00";
    private static final String CONTENT_TYPE_MULTIPART = "multipart/form-data; boundary="
            + BOUNDARY;

    protected abstract void parseResponse(RemoteResponse response);

    protected abstract String buildUrl();

    protected abstract HttpMethod getHttpMethod();

    protected Map<String, String> buildHeaders() {
        return null;
    }

    public HttpRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    public void doWork() {
        try {
            HttpURLConnection urlConnection = createConnection(buildUrl());
            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            writeToStream(out, getPostData());
           // writeStream(out);

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            parseResponse(new RemoteResponse(in, urlConnection.getResponseCode()));
        } catch (Exception e) {
            e.printStackTrace();
            onError(e);
        }
    }

    private void writeToStream(OutputStream out, String data) throws IOException {
        if(isEmpty(data)) return;
        DataOutputStream printout = new DataOutputStream(out);
        printout.writeBytes(URLEncoder.encode(data, CHARSET_UTF8));
        printout.flush ();
        printout.close ();
    }

    protected String getPostData(){
        return null;
    }

    @Deprecated
    protected void writeStream(OutputStream out) {

    }



    protected HttpURLConnection createConnection(String urlString) throws Exception {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection;
        if (isHttps()) {
            HttpsURLConnection httpsUrlConnection = (HttpsURLConnection) url.openConnection();
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{new ExX509TrustManager()}, null);
            httpsUrlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
            urlConnection = httpsUrlConnection;
        } else {
            urlConnection = (HttpURLConnection) url.openConnection();
        }
        urlConnection.setReadTimeout(TIMEOUT * 2);
        urlConnection.setConnectTimeout(TIMEOUT);
        urlConnection.setRequestMethod(getHttpMethod().name());
        urlConnection.setDoOutput(getHttpMethod().isDoOutPut());
        urlConnection.setRequestProperty(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON);
        urlConnection.setChunkedStreamingMode(0);
        Map<String, String> headers = buildHeaders();
        if (headers != null) {
            for (String key : headers.keySet()) {
                urlConnection.setRequestProperty(key, headers.get(key));
            }
        }
        return urlConnection;
    }




    protected final boolean isHttps() {
        return buildUrl().startsWith("https");
    }
}
