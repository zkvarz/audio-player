package com.core.structure.requests.abs.urlconnection;

import com.core.structure.models.result.IResult;
import com.core.structure.requests.mock.ICallback;


public abstract class DeleteRequest<T extends IResult> extends HttpRequest<T> {

    public DeleteRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpMethod getHttpMethod() {
        return HttpMethod.DELETE;
    }


}
