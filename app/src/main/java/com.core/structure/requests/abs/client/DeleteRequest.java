package com.core.structure.requests.abs.client;

import com.core.structure.requests.abs.client.base.QueryRequest;
import com.core.structure.requests.mock.ICallback;


public abstract class DeleteRequest<T> extends QueryRequest<T> {

    public DeleteRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpRequestFactory.HttpMethod getHttpMethod() {
        return HttpRequestFactory.HttpMethod.DELETE;
    }


}
