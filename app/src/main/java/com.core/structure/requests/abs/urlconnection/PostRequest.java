package com.core.structure.requests.abs.urlconnection;

import com.core.structure.requests.mock.ICallback;

public abstract class PostRequest<T> extends HttpRequest<T> {

    public PostRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpMethod getHttpMethod() {
        return HttpMethod.POST;
    }
}
