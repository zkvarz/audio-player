package com.core.structure.requests.abs.urlconnection;

public enum HttpMethod {
    GET(false), POST(true), PUT(true), DELETE(false);

    private final boolean doOutPut;

    HttpMethod(boolean doOutPut) {
        this.doOutPut = doOutPut;
    }

    public boolean isDoOutPut() {
        return doOutPut;
    }
}