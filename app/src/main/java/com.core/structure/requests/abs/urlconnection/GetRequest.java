package com.core.structure.requests.abs.urlconnection;

import com.core.structure.requests.mock.ICallback;

import java.io.OutputStream;


public abstract class GetRequest<T> extends HttpRequest<T> {

    @Override
    protected final void writeStream(OutputStream out) {
    }

    public GetRequest(ICallback<T> callback) {
        super(callback);
    }

    @Override
    protected final HttpMethod getHttpMethod() {
        return HttpMethod.GET;
    }

}
