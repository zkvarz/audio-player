package com.core.structure.manager;

import com.core.structure.models.result.IResult;

public abstract class Manager<T extends IResult> extends AbsManager<T> {

//    /**
//     * The list of observers.  An observer can be in the list at most
//     * once and will never be null.
//     */
//    protected final ArrayList<InterfaceObserver<T>> mObservers = new ArrayList<>();
//
//
//    @Override
//    public void registerObserver(InterfaceObserver<T> observer) {
//        synchronized (mObservers) {
//            if (!mObservers.contains(observer)) {
//                mObservers.add(observer);
//            }
//        }
//    }
//
//    @Override
//    public void unregisterAll() {
//        synchronized (mObservers) {
//            mObservers.clear();
//        }
//    }
//
//    @Override
//    public void unregisterObserver(InterfaceObserver<T> observer) {
//        synchronized (mObservers) {
//            int index = mObservers.indexOf(observer);
//            if (index != -1) {
//                mObservers.delete(index);
//            }
//        }
//    }
//
//
//    @Override
//    public void notifySuccess(final T result) {
//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                for (InterfaceObserver<T> observer : mObservers) {
//                    if (observer != null) {
//                        observer.onObserved(result);
//                    }
//                }
//            }
//        });
//    }
}

