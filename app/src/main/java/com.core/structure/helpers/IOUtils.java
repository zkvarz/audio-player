package com.core.structure.helpers;

import android.graphics.Bitmap;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class IOUtils {

	public static byte[] toByteArray(InputStream inputStream) throws IOException {
		// this dynamically extends to take the bytes you read
		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		// this is storage overwritten on each iteration with bytes
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];

		// we need to know how may bytes were read to write them to the
		// byteBuffer
		int len;
		while ((len = inputStream.read(buffer)) != -1) {
			byteBuffer.write(buffer, 0, len);
		}

		// and then we can return your byte array.
		return byteBuffer.toByteArray();
	}

	public static String toString(InputStream is) throws IOException {
		StringBuilder result = new StringBuilder();
		InputStreamReader in = new InputStreamReader(is);

		// Load the results into a StringBuilder
		int read;
		char[] buff = new char[1024];
		while ((read = in.read(buff)) != -1) {
			result.append(buff, 0, read);
		}
		return result.toString();
	}

    public static InputStream toStream(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return new ByteArrayInputStream(stream.toByteArray());
    }
}
