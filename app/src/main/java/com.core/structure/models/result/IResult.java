package com.core.structure.models.result;


import com.core.structure.models.model.IModel;

public interface IResult<T extends IModel> {

 //   public T getModel();

    public boolean isSuccess();

 //   public IError getError();

}
