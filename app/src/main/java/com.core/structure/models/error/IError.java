package com.core.structure.models.error;

public interface IError {

    public String getErrorMessage();
}
