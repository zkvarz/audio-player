package com.core.structure.ui.dialogs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import com.zkvarz.audioplayer.R;


/**
 * progress dialog with only one spinner
 */
public class SimpleProgressDialog extends ProgressDialog {

    public SimpleProgressDialog(Context context) {
        super(context, R.style.App_Theme_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        setCancelable(false);
    }
}
