package com.core.structure.ui.fragments.abs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.core.structure.helpers.Dialogs;
import com.core.structure.ui.activities.ContentActivity;
import com.core.structure.ui.dialogs.SimpleProgressDialog;

/**
 * base for all others fragments class
 */
public abstract class BaseFragment extends DialogFragment {



    public interface OnDismissListener {
        void onDismiss(DialogFragment dialogFragment);
    }

    private OnDismissListener onDismissListener;

    protected ProgressDialog mProgressDialog;

    protected abstract View initView(LayoutInflater inflater, ViewGroup container);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = getProgressDialog(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String title = getStringById(getTitleResId());
        setHasOptionsMenu(true);
//        if (getSupportActionBar() != null && title != null) {
//            getSupportActionBar().setTitle(getString(getTitleResId()));
//        }
        return initView(inflater, container);
    }

    protected int getTitleResId() {
        return 0;
    }

    protected ContentActivity getContentActivity() {
//        return (ContentActivity) getActivity();
        return null;
    }

    @Override
    public void onDestroy() {
        safeClose(mProgressDialog);
        super.onDestroy();
    }

    protected void finish() {
        getActivity().finish();
    }

    protected void showErrorDialog(int message) {
        showErrorDialog(getString(message));
    }

    protected void showErrorDialog(String message) {
        Dialogs.showSingleButtonDialog(getActivity(), message);
    }

    protected void showErrorDialog(int message, int title) {
        Dialogs.showSingleButtonDialog(getActivity(), message, title);
    }

    protected static ProgressDialog getProgressDialog(Context context) {
        return new SimpleProgressDialog(context);
    }

    protected void safeClose(Dialog dialog) {
        Dialogs.safeClose(dialog);
    }


    protected void onBackPressed() {
        getActivity().onBackPressed();
    }

    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

//    protected ActionBar getSupportActionBar() {
//        return getContentActivity().getSupportActionBar();
//    }

    protected String getStringById(int resId) {
        if (resId <= 0) return null;
        try {
            return getString(resId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

//    protected void setupHomeButton(boolean enable) {
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setHomeButtonEnabled(enable);
//            actionBar.setDisplayHomeAsUpEnabled(enable);
//        }
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        setupHomeButton(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item != null && item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    protected void showAsDialog(DialogFragment dialogFragment, int style, int theme) {
//        getContentActivity().showAsDialog(dialogFragment, style, theme);
    }

    protected void closeAsDialog(boolean allowStateLoss) {
//        getContentActivity().closeAsDialog(allowStateLoss);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (getOnDismissListener() != null) {
            getOnDismissListener().onDismiss(this);
            setOnDismissListener(null);
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
        if (getOnDismissListener() != null) {
            getOnDismissListener().onDismiss(this);
            setOnDismissListener(null);
        }
    }

    public OnDismissListener getOnDismissListener() {
        return onDismissListener;
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
    }

}
