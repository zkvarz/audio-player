
package com.core.structure.ui.fragments.abs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.core.structure.helpers.Logger;


public abstract class LifeCircleFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.debug(getClass(), "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.debug(getClass(), "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Logger.debug(getClass(), "onViewCreated");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Logger.debug(getClass(), "onCreateContextMenu");
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.debug(getClass(), "onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.debug(getClass(), "onResume");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Logger.debug(getClass(), "onSaveInstanceState");
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.debug(getClass(), "onSaveInstanceState");
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.debug(getClass(), "onSaveInstanceState");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Logger.debug(getClass(), "onSaveInstanceState");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.debug(getClass(), "onSaveInstanceState");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Logger.debug(getClass(), "onDetach");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.debug(getClass(), "onActivityCreated");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Logger.debug(getClass(), "onAttach");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Logger.debug(getClass(), "onDestroyView");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Logger.debug(getClass(), "onViewStateRestored");
    }


}

