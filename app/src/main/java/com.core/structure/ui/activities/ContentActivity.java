package com.core.structure.ui.activities;

import android.os.Bundle;

import com.core.structure.helpers.Dialogs;
import com.zkvarz.audioplayer.R;


/**
 * Activity with only one fullscreen fragment
 */
public class ContentActivity extends BaseActivity {


    private static final String DIALOG = "dialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    protected void init() {
        setContentView(R.layout.activity_content);
        addFragment();
    }

    protected void addFragment() {

    }
/*
    public void showAsDialog(DialogFragment dialogFragment, int style, int theme) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(DIALOG);
        if (prev != null) {
            if (prev instanceof DialogFragment) {
                ((DialogFragment) prev).dismiss();
            }
            ft.remove(prev);
        }
        dialogFragment.setStyle(style, theme);
        dialogFragment.show(ft, DIALOG);
    }

    public void closeAsDialog(boolean allowStateLoss) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag(DIALOG);
        if (prev != null) {
            if (prev instanceof BaseFragment) {
                if (allowStateLoss)
                    ((BaseFragment) prev).dismissAllowingStateLoss();
                else
                    ((BaseFragment) prev).dismiss();
            }
            ft.remove(prev);
        }
        if (allowStateLoss)
            ft.commitAllowingStateLoss();
        else
            ft.commit();
    }*/

    public int getContainerId() {
        return R.id.container;
    }

    protected void showErrorDialog(String message) {
        Dialogs.showSingleButtonDialog(this, message);
    }

}
