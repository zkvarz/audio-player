package com.core.structure.ui.activities;


import android.content.Intent;

import com.zkvarz.audioplayer.R;


public class ContentResultActivity extends ContentActivity {

	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.stay,
				R.anim.slide_top_to_bottom_out);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
}
