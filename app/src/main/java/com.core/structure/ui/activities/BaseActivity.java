package com.core.structure.ui.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.core.helpers.FileHelper;
import com.core.images.SaveImageRequest;
import com.core.structure.helpers.Dialogs;
import com.core.structure.models.error.IError;
import com.core.structure.requests.mock.ICallback;
import com.core.structure.ui.dialogs.SimpleProgressDialog;
import com.core.structure.ui.fragments.abs.BaseFragment;
import com.zkvarz.audioplayer.R;

import java.io.File;

import static android.text.TextUtils.isEmpty;
import static com.core.helpers.ContentUtils.getPath;

public abstract class BaseActivity extends Activity {

    protected Dialog mProgressDialog;
    private static final String EXTRA_IMAGE = "extra_image";
    private static final String EXTRA_CROPPING = "extra_crop";
    private static boolean cropPhoto = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressDialog = new SimpleProgressDialog(this);
        if (savedInstanceState != null) {
            String filePath = savedInstanceState.getString(EXTRA_IMAGE);
            if (!isEmpty(filePath)) {
                sPhotoFile = new File(filePath);
            }
            cropPhoto = savedInstanceState.getBoolean(EXTRA_CROPPING);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (sPhotoFile != null) {
            outState.putString(EXTRA_IMAGE, sPhotoFile.getAbsolutePath());
            outState.putBoolean(EXTRA_CROPPING, cropPhoto);
        }
    }

    public void add(BaseFragment fragment, int container) {
        getFragmentTransaction().add(container, fragment,
                fragment.getClass().getSimpleName()).commit();
    }

    public void replace(BaseFragment fragment, int container) {
        getFragmentTransaction().replace(container, fragment,
                fragment.getClass().getSimpleName()).commit();
    }


    public void replaceInBackStack(BaseFragment fragment, int container) {
        getFragmentTransaction()
                .replace(container, fragment,
                        fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName()).commit();
    }

    public void replaceInBackStackWithAnim(BaseFragment fragment, int container) {
        getFragmentTransaction()
                .setCustomAnimations(
                        R.anim.slide_right_to_left_in,
                        R.anim.slide_right_to_left_out,
                        R.anim.stay,
                        R.anim.slide_left_to_right_out)
                .replace(container, fragment,
                        fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    public void replaceWithAnim(BaseFragment fragment, int container) {
        getFragmentTransaction()
                .setCustomAnimations(
                        R.anim.slide_right_to_left_in,
                        R.anim.slide_right_to_left_out,
                        R.anim.stay,
                        R.anim.slide_left_to_right_out)
                .replace(container, fragment,
                        fragment.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public void addToBackStack(BaseFragment fragment, int container) {
        getFragmentTransaction()
                .add(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(fragment.getClass().getSimpleName()).commit();
    }

    public FragmentTransaction getFragmentTransaction() {
//        return getSupportFragmentManager().beginTransaction();
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void hideNavigationBar() {
        View decorView = getWindow().getDecorView();
        // Hide both the navigation bar and the status bar.
        // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
        // a general rule, you should design your app to hide the status bar whenever you
        // hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void hideStatusBar() {
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public void dimSystemBars() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LOW_PROFILE;
        decorView.setSystemUiVisibility(uiOptions);
    }

    protected void safeClose(Dialog dialog) {
        Dialogs.safeClose(dialog);
    }

    private static final String MIME_TYPE_IMAGE = "image/*";
    public static final int REQUEST_IMAGE_PICK = 1;
    public static final int REQUEST_IMAGE_CROP = 2;

    private static final String ACTION_CROP = "com.android.camera.action.CROP";

    private static final String EXTRA_RETURN_DATA = "return-data";
    private static final String EXTRA_NO_FACE_DETECTION = "noFaceDetection";
    private static final String EXTRA_CROP = "crop";
    public static final String EXTRA_SET_WALLPAPER = "setWallpaper";
    private static final String EXTRA_SCALE = "scale";
    private static final String EXTRA_OUTPUT_Y = "outputY";
    private static final String EXTRA_OUTPUT_X = "outputX";
    private static final String EXTRA_ASPECT_Y = "aspectY";
    private static final String EXTRA_ASPECT_X = "aspectX";
    private static final String TEMP_IMAGE_EXT = ".jpg";
    private static final int AREA_IMAGE_SIZE = 450;


    private static File sPhotoFile;

    File createTempFile() {
        sPhotoFile = FileHelper.createCacheFile(getApplicationContext(), FileHelper.createUniqueFileName(getApplicationContext()) + ".png");
        return sPhotoFile;
    }

    private static File getTempFile() {
        return sPhotoFile;
    }

    protected void pickImage() {
        pickImage(createTempFile());
    }

    protected void pickImageWithoutCropping() {
        cropPhoto=false;
        pickImage();
    }

    private void pickImage(File imageFile) {
        Intent imageIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
        imageIntent.setType(MIME_TYPE_IMAGE);
        imageIntent.addCategory(Intent.CATEGORY_OPENABLE);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(imageFile));
        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, imageIntent);
        Intent[] intentArray = {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        chooser.putExtra(EXTRA_RETURN_DATA, Uri.fromFile(imageFile));
        startActivityForResult(chooser, REQUEST_IMAGE_PICK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_IMAGE_CROP:
                    deliverImagePath(getPath(getApplicationContext(), Uri.fromFile(getTempFile())));
                    break;
                case REQUEST_IMAGE_PICK:
                    if (data != null && data.getData() != null) {
                        Uri uri = data.getParcelableExtra(EXTRA_RETURN_DATA);
                        mProgressDialog.show();
                        new SaveImageRequest(data.getData(), getTempFile(), new ICallback<Boolean>() {
                            @Override
                            public void onSuccess(Boolean aBoolean) {
                                safeClose(mProgressDialog);
                                cropPhoto(Uri.fromFile(getTempFile()));
                            }

                            @Override
                            public void onError(IError error) {
                                safeClose(mProgressDialog);
                            }
                        }).execute();
                    } else {
                        cropPhoto(Uri.fromFile(getTempFile()));
                    }
                    break;
            }
        }
    }

    /**
     * Method try to crop image obtained from gallery or camera
     */
    private void cropPhoto(Uri imageUri) {
        if(!cropPhoto){
            deliverImagePath(getPath(getApplicationContext(),imageUri));
            return;
        }
        createTempFile();
        Intent intent = getCropIntent(imageUri,
                Uri.fromFile(getTempFile()));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CROP);
        } else {
            Intent intent1 = new Intent();
            intent1.setData(imageUri);
            onActivityResult(REQUEST_IMAGE_CROP, Activity.RESULT_OK, intent1);
        }

    }

    public static Intent getCropIntent(Uri inputUri, Uri outputUri) {
        Intent intent = new Intent(ACTION_CROP);
        intent.setDataAndType(inputUri, MIME_TYPE_IMAGE);
        intent.putExtra(EXTRA_CROP, true);

        intent.putExtra(EXTRA_ASPECT_X, 1);
        intent.putExtra(EXTRA_ASPECT_Y, 1);
        intent.putExtra(EXTRA_OUTPUT_X, AREA_IMAGE_SIZE);
        intent.putExtra(EXTRA_OUTPUT_Y, AREA_IMAGE_SIZE);
        intent.putExtra(EXTRA_SCALE, true);
        intent.putExtra(EXTRA_NO_FACE_DETECTION, true);
        intent.putExtra(EXTRA_SET_WALLPAPER, false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
        intent.putExtra(EXTRA_RETURN_DATA, false); // false - save to path; true
        // - return limited-size
        // bitmap in intent
        return intent;
    }

    //realize this method to process picked image/photo path
    protected void deliverImagePath(String path) {

    }
}
