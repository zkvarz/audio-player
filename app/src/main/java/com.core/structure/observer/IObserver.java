package com.core.structure.observer;


import com.core.structure.models.error.IError;

public interface IObserver<T> {

    void onObserved(T t);

    void onNothing(IError error);
}
