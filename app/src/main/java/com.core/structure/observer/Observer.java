package com.core.structure.observer;


import com.core.structure.models.result.Result;

public class Observer<T extends Result> extends AbsObserver<T> {

    @Override
    public void onObserved(T t) {
        if (!t.isSuccess()) {
            onNothing(t.getError());
        } else {
            onAnyResponse();
        }
    }
}
