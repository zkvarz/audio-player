package com.core.structure.observer;


import com.core.structure.models.error.IError;

public class AbsObserver<T> implements IObserver<T> {

    @Override
    public void onObserved(T t) {
        onAnyResponse();
    }

    @Override
    public void onNothing(IError error) {
        onAnyResponse();
    }


    public void onAnyResponse() {
    }
}
