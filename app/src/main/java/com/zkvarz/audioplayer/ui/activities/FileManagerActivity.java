package com.zkvarz.audioplayer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.core.structure.helpers.Logger;
import com.zkvarz.audioplayer.R;
import com.zkvarz.audioplayer.ui.adapters.FileManagerAdapter;

import java.io.File;
import java.util.Arrays;

import static com.zkvarz.audioplayer.utilities.SongUtilities.isMusicFile;

public class FileManagerActivity extends Activity {

    private ListView mListView;
    private TextView mPathDisplay;
    private FileManagerAdapter mListAdapter;
    private String mCurrentPath;

    public static final String FOLDER_PATH = "FOLDER_PATH";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        setTitle(R.string.fileManager);

        mCurrentPath = getFilesystemBrowseStart().getAbsolutePath();
        mListAdapter = new FileManagerAdapter(this, 0);
        mPathDisplay = (TextView) findViewById(R.id.path_display);
        mListView = (ListView) findViewById(R.id.list);


        mListView.setAdapter(mListAdapter);

        Button mSaveButton = (Button) findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Logger.debug(getClass(), "path chosen " + mCurrentPath);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                intent.putExtra(FOLDER_PATH, mCurrentPath);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_file_manager, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.action_about:
                Toast.makeText(getApplicationContext(), R.string.meAuthor, Toast.LENGTH_LONG).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /*
    ** Called when we are displayed (again)
    ** This will always refresh the whole song list
    */
    @Override
    public void onResume() {
        super.onResume();
        refreshDirectoryList();
    }

    /**
     * Called by FileSystem adapter to get the start folder
     * for browsing directories
     */
    protected File getFilesystemBrowseStart() {
//        SharedPreferences prefs = PlaybackService.getSettings(this);
//        String folder = prefs.getString(PrefKeys.FILESYSTEM_BROWSE_START, "");
        String folder = "";
        File fs_start = new File(folder.equals("") ? Environment.getExternalStorageDirectory().getAbsolutePath() : folder);
        return fs_start;
    }

    /*
    ** Enters selected directory at 'pos'
	*/
    public void onDirectoryClicked(int pos) {
        String dirent = mListAdapter.getItem(pos);

//        File path = new File(mCurrentPath);
        if (isMusicFile(mListAdapter.getItem(pos))) {
            Logger.debug(getClass(), "onDirectoryClicked nope");
            Logger.debug(getClass(), "");
            return;
        }

        if (pos == 0) {
            Logger.debug(getClass(), "onDirectoryClicked pos == 0");
            mCurrentPath = (new File(mCurrentPath)).getParent();
        } else {
            Logger.debug(getClass(), "onDirectoryClicked pos != 0");

            mCurrentPath += "/" + dirent;
        }

		/* let java fixup any strange paths */
        mCurrentPath = (new File(mCurrentPath == null ? "/" : mCurrentPath)).getAbsolutePath();

        refreshDirectoryList();
    }

    /*
    ** display mCurrentPath in the dialog
    */
    private void refreshDirectoryList() {
        File path = new File(mCurrentPath);
        File[] dirs = path.listFiles();

        mListAdapter.clear();
        mListAdapter.add("../");

        if (dirs != null) {
            Arrays.sort(dirs);
            for (File fentry : dirs) {
                if (fentry.isDirectory()) {
                    mListAdapter.add(fentry.getName());
                } else {
                    if (isMusicFile(fentry)) {
                        mListAdapter.add(fentry.getName());
                    }
                }
            }
        } else {
            Toast.makeText(this, "Failed to display " + mCurrentPath, Toast.LENGTH_SHORT).show();
        }
        mPathDisplay.setText(mCurrentPath);
        mListView.setSelectionFromTop(0, 0);
    }

}
