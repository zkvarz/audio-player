/*
 * Copyright (C) 2013-2015 Adrian Ulrich <adrian@blinkenlights.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. 
 */

package com.zkvarz.audioplayer.ui.adapters;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zkvarz.audioplayer.R;
import com.zkvarz.audioplayer.ui.activities.FileManagerActivity;

import static com.zkvarz.audioplayer.utilities.SongUtilities.isMusicFile;

public class FileManagerAdapter extends ArrayAdapter<String>
        implements View.OnClickListener {

    private final FileManagerActivity mActivity;
    private Drawable mFolderIcon;
    private Drawable mNoteIcon;

    public FileManagerAdapter(FileManagerActivity activity, int resource) {
        super(activity, resource);
        mActivity = activity;
        mFolderIcon = ContextCompat.getDrawable(getContext(), R.drawable.folder);
        mNoteIcon = ContextCompat.getDrawable(getContext(), R.drawable.note_icon);
    }

    public class ViewHolder {
        public long id;
        public String title;
        public TextView text;
        public View divider;
        public ImageView arrow;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;

        if (convertView == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.library_row_expandable, parent, false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.text);
            holder.divider = view.findViewById(R.id.divider);
            holder.arrow = (ImageView) view.findViewById(R.id.arrow);
            holder.text.setOnClickListener(this);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        String label = getItem(pos);
        holder.id = pos;
        holder.text.setText(label);
        holder.divider.setVisibility(View.GONE);
        holder.arrow.setVisibility(View.GONE);

        if (isMusicFile(label)) {
            holder.text.setCompoundDrawablesWithIntrinsicBounds(mNoteIcon, null, null, null);
        } else {
            holder.text.setCompoundDrawablesWithIntrinsicBounds(mFolderIcon, null, null, null);
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        ViewHolder holder = (ViewHolder) ((View) view.getParent()).getTag();
        mActivity.onDirectoryClicked((int) holder.id);
    }

}
