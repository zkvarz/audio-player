package com.zkvarz.audioplayer.ui.adapters;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.core.structure.helpers.Logger;
import com.zkvarz.audioplayer.R;
import com.zkvarz.audioplayer.manager.SongsManager;
import com.zkvarz.audioplayer.models.Song;
import com.zkvarz.audioplayer.utilities.SongUtilities;

import java.util.List;

/**
 * Created by kvarivoda on 10.09.2015.
 */
public class SongListAdapter extends ArrayAdapter<Song> {

    private List<Song> list;
    private Context context;

    private int currentlyPlaying = -1;

    public SongListAdapter(Context context,
                           List<Song> list) {
        super(context, 0, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Song getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder {
        TextView textViewTitle;
        TextView textViewAuthor;
        TextView textViewAlbum;
        TextView textViewDate;
        ImageView imageViewSong;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.song_item, parent, false);
            holder = new ViewHolder();
            holder.textViewTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
            holder.textViewAuthor = (TextView) convertView.findViewById(R.id.textViewAuthor);
            holder.textViewAlbum = (TextView) convertView.findViewById(R.id.textViewAlbum);
            holder.textViewDate = (TextView) convertView.findViewById(R.id.textViewDate);
            holder.imageViewSong = (ImageView) convertView.findViewById
                    (R.id.imageViewSong);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Song songItem = getItem(position);

        if (songItem != null) {
            if (!SongsManager.isSongDetailsObtained()) {
                Logger.debug(getClass(), "not yet isSongDetailsObtained");
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.parse(songItem.getPath());
                mediaMetadataRetriever.setDataSource(context, uri);

                String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                String composer = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                String album = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                long duration = Long.valueOf(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

                if (!TextUtils.isEmpty(title)) {
                    songItem.setTitle(title);
                }

                if (!TextUtils.isEmpty(composer)) {
                    songItem.setComposer(composer);
                }

                if (!TextUtils.isEmpty(album)) {
                    songItem.setAlbum(album);
                }

                songItem.setDuration(duration);
            } else {
                Logger.debug(getClass(), "SongsManager.isSongDetailsObtained!");
            }

            Logger.debug(getClass(), "real data title is " + songItem.getTitle());
            Logger.debug(getClass(), "real data author is " + songItem.getComposer());
            Logger.debug(getClass(), "real data album is " + songItem.getAlbum());
            Logger.debug(getClass(), "real data duration is " + songItem.getDuration());
            Logger.debug(getClass(), "real data duration songPath is " + songItem.getPath());


            if (!TextUtils.isEmpty(songItem.getTitle())) {
                holder.textViewTitle.setText(songItem.getTitle());
            } else if (!TextUtils.isEmpty(songItem.getFileName())) {
                holder.textViewTitle.setText(songItem.getFileName());
            } else {
                holder.textViewTitle.setText("");
            }

            if (!TextUtils.isEmpty(songItem.getComposer())) {
                holder.textViewAuthor.setText(songItem.getComposer());
            } else {
                holder.textViewAuthor.setText("");
            }

            if (!TextUtils.isEmpty(songItem.getAlbum())) {
                holder.textViewAlbum.setText(songItem.getAlbum());
            } else {
                holder.textViewAlbum.setText("");
            }

            SongUtilities utils = new SongUtilities();
            if (songItem.getDuration() > 0) {
                holder.textViewDate.setText(utils.milliSecondsToTimer(songItem.getDuration()));
            } else {
                holder.textViewDate.setText("");
            }

            if (position == currentlyPlaying) {
                holder.imageViewSong.setImageResource(R.drawable.note_icon);
            } else {
                holder.imageViewSong.setImageResource(R.drawable.play_icon);
            }

        }
        return convertView;
    }

    public void setCurrentlyPlaying(int currentlyPlaying) {
        this.currentlyPlaying = currentlyPlaying;
    }
}