package com.zkvarz.audioplayer.ui.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.core.structure.helpers.Logger;
import com.core.structure.ui.activities.ContentActivity;
import com.zkvarz.audioplayer.R;
import com.zkvarz.audioplayer.manager.SongsManager;
import com.zkvarz.audioplayer.models.Song;
import com.zkvarz.audioplayer.services.MusicService;
import com.zkvarz.audioplayer.services.MusicService.MusicBinder;
import com.zkvarz.audioplayer.ui.adapters.SongListAdapter;
import com.zkvarz.audioplayer.utilities.SongUtilities;

import java.util.ArrayList;
import java.util.List;

import static android.text.TextUtils.isEmpty;
import static com.zkvarz.audioplayer.utilities.SongUtilities.getSongName;

/**
 * Created by kvarivoda on 10.09.2015.
 */
public class MainActivity extends ContentActivity implements View.OnClickListener,
        SongsManager.OnSongsFetchedListener, SeekBar.OnSeekBarChangeListener, SearchView.OnQueryTextListener {

    private SongListAdapter adapter;
    private ImageButton btnPlay;
    private ImageButton btnStop;
    private ImageButton btnForward;
    private ImageButton btnBackward;
    private ImageButton btnNext;
    private ImageButton btnPrevious;
    private ImageButton btnRepeat;
    private ImageButton btnShuffle;
    private SeekBar songProgressBar;
    private TextView songTitleLabel;
    private TextView textViewAuthor;
    private TextView textViewAlbum;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;

    private static ArrayList<Song> songSearchList = new ArrayList<>();

    //service
    private MusicService musicService;
    private Intent playIntent;
    //binding to service
    private boolean isBound = false;

    private BroadcastReceiver receiver;

    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();

    private SongUtilities utils;
    private int seekForwardTime = 5000; // 5000 milliseconds
    private int seekBackwardTime = 5000; // 5000 milliseconds

    private int pickedDialogOrder = 0;
    private String currentSongUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button playAll = (Button) findViewById(R.id.playAll);
        playAll.setOnClickListener(this);

        Button openFolder = (Button) findViewById(R.id.openFolder);
        openFolder.setOnClickListener(this);

        ListView songListView = (ListView) findViewById(R.id.usersListView);
        adapter = new SongListAdapter(this, songSearchList);
        songListView.setAdapter(adapter);
        songListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long l) {

                // check if next song is there or not
                if (position < (songSearchList.size())) {
                    adapter.setCurrentlyPlaying(position);
                    musicService.setList(songSearchList);
                    musicService.playSong(position);

                    Logger.debug(getClass(), "print all the data");
                    Song song = songSearchList.get(position);
                    Logger.debug(getClass(), "real data title is " + song.getTitle());
                    Logger.debug(getClass(), "real data author is " + song.getComposer());
                    Logger.debug(getClass(), "real data album is " + song.getAlbum());
                    Logger.debug(getClass(), "real data duration is " + song.getDuration());
                    Logger.debug(getClass(), "real data fileName is " + song.getFileName());
                    Logger.debug(getClass(), "real data duration songPath is " + song.getPath());
                }
            }
        });

        // All player buttons
        btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        btnStop = (ImageButton) findViewById(R.id.btnStop);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        btnBackward = (ImageButton) findViewById(R.id.btnBackward);
        btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        btnRepeat = (ImageButton) findViewById(R.id.btnRepeat);
        btnShuffle = (ImageButton) findViewById(R.id.btnShuffle);
        songProgressBar = (SeekBar) findViewById(R.id.songProgressBar);
        songTitleLabel = (TextView) findViewById(R.id.songTitle);
        textViewAuthor = (TextView) findViewById(R.id.textViewAuthor);
        textViewAlbum = (TextView) findViewById(R.id.textViewAlbum);

        songCurrentDurationLabel = (TextView) findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) findViewById(R.id.songTotalDurationLabel);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // do something here.
                Logger.debug(getClass(), "BroadcastReceiver onReceive!");

                updateSongUi();

                // Changing Button Image to pause image
                btnPlay.setImageResource(R.drawable.pause_action);

                if (musicService.isPlaying()) {
                    btnPlay.setImageResource(R.drawable.pause_action);
                } else {
                    btnPlay.setImageResource(R.drawable.play_action);
                }

                // Updating progress bar
                updateProgressBar();

            }
        };

    }

    //start and bind the service when the activity starts
    @Override
    protected void onStart() {
        initializeMediaPlayer();
        Logger.debug(getClass(), "onStart bind");
        super.onStart();
        if (playIntent == null) {
            Logger.debug(getClass(), "onStart playIntent null");
            Logger.debug(getClass(), "onStart isBound " + isBound);
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(MusicService.BROADCAST_UI_UPDATE)
        );
    }

    //connect to the service
    private ServiceConnection musicConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicBinder binder = (MusicBinder) service;
            //get service
            musicService = binder.getService();
            //pass list
            musicService.setList(songSearchList);
            musicService.setCurrentSongIndex(0);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    private void handleIncomingContent(Intent intent) {
        Logger.debug(getClass(), "handleIncomingContent ");
        // Get the Intent action
        String action = intent.getAction();

        /*
         * For ACTION_VIEW, the Activity is being asked to display data.
         * Get the URI.
         */
        if (TextUtils.equals(action, Intent.ACTION_VIEW)) {
            // Get the URI from the Intent
            Uri beamUri = intent.getData();

            Logger.debug(getClass(), "handleIncomingContent beamUri " + beamUri);

            String songPath = getRealPathFromURI(beamUri);
            Logger.debug(getClass(), "handleIncomingContent getRealPathFromURI " + songPath);

            //check if the same song
            if (!isEmpty(currentSongUri) && currentSongUri.equals(songPath)) {
                Logger.debug(getClass(), "currentSongUri.equals(songPath)");

                //if the same song and not playing play again
            } else {
                Logger.debug(getClass(), "currentSongUri.equals(songPath) false");
                SongsManager.getInstance().fetchSongsByFolder(MainActivity.this, getApplicationContext(), songPath);
            }
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        result = result.replaceAll("%20", " ");
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        // Here we getInstance the action view we defined
        MenuItem actionSearchItem = menu.findItem(R.id.action_search);
        SearchView actionSearchView = (SearchView) actionSearchItem.getActionView();

        // We then getInstance the edit text view that is part of the action view
        if (actionSearchView != null) {
            actionSearchView.setOnQueryTextListener(this);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_about:
                // Code you want run when activity is clicked
                Toast.makeText(getApplicationContext(), R.string.meAuthor, Toast.LENGTH_LONG).show();
                return true;
            case R.id.action_filter:
                showFilterDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showFilterDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.filter_dialog);
        dialog.setTitle(R.string.filterBy);

        final RadioButton filterTitle = (RadioButton) dialog.findViewById(R.id.filterTitle);
        final RadioButton filterAuthor = (RadioButton) dialog.findViewById(R.id.filterAuthor);
        final RadioButton filterAlbum = (RadioButton) dialog.findViewById(R.id.filterAlbum);
        final RadioButton filterDuration = (RadioButton) dialog.findViewById(R.id.filterDuration);

        if (pickedDialogOrder == 0) {
            filterTitle.setChecked(true);
        } else if (pickedDialogOrder == 1) {
            filterAuthor.setChecked(true);
        } else if (pickedDialogOrder == 2) {
            filterAlbum.setChecked(true);
        } else if (pickedDialogOrder == 3) {
            filterDuration.setChecked(true);
        }

        Button dialogOk = (Button) dialog.findViewById(R.id.dialogOk);
        dialogOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterTitle.isChecked()) {
                    SongsManager.getInstance().sortByTitle(MainActivity.this, getApplicationContext());
                    pickedDialogOrder = 0;
                } else if (filterAuthor.isChecked()) {
                    SongsManager.getInstance().sortByParam(MainActivity.this, getApplicationContext(), SongsManager.BY_AUTHOR);
                    pickedDialogOrder = 1;
                } else if (filterAlbum.isChecked()) {
                    SongsManager.getInstance().sortByParam(MainActivity.this, getApplicationContext(), SongsManager.BY_ALBUM);
                    pickedDialogOrder = 2;
                } else if (filterDuration.isChecked()) {
                    SongsManager.getInstance().sortByDuration(MainActivity.this, getApplicationContext());
                    pickedDialogOrder = 3;
                }
                dialog.dismiss();
            }
        });

        Button dialogCancel = (Button) dialog.findViewById(R.id.dialogCancel);
        dialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.playAll:
                mProgressDialog.show();
                SongsManager.getInstance().fetchAllSongs(this, getApplicationContext());
                break;
            case R.id.openFolder:
                mProgressDialog.show();
                Intent intent = new Intent(this, FileManagerActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void stopMediaPlayer() {
        if (musicService != null && musicService.isMediaPlayerExist()) {
            mHandler.removeCallbacks(mUpdateTimeTask);
            mHandler = null;
            musicService.stopAction();

            mHandler = new Handler();
        }
    }

    @Override
    public void onSongsFetched(final List<Song> fetchedSongList) {
        Logger.debug(getClass(), "fetchedSongList size " + fetchedSongList.size());
        initializeMediaPlayer();
        stopMediaPlayer();

        songSearchList.clear();
        songSearchList.addAll(fetchedSongList);
        adapter.notifyDataSetChanged();
        safeClose(mProgressDialog);

        Logger.debug(getClass(), "updating adapter");

        adapter.setCurrentlyPlaying(0);
        if (musicService != null) {
            Logger.debug(getClass(), "musicService != null");
            musicService.setList(fetchedSongList);
            musicService.playSong(0);
        } else {
            Logger.error(getClass(), "musicService == null");
        }
    }

    @Override
    public void onSongsDetailsFetched(List<Song> songList) {
        adapter.notifyDataSetChanged();
        safeClose(mProgressDialog);
    }

    private void updateSongUi() {
        adapter.setCurrentlyPlaying(musicService.getCurrentSongIndex());
        adapter.notifyDataSetChanged();

        if (!songSearchList.isEmpty() && (musicService.getCurrentSongIndex()) < songSearchList.size()) {
            initializeSongDetails(songSearchList.get(musicService.getCurrentSongIndex()).getPath());
        } else {
            Logger.error(getClass(), "ArrayIndexOutOfBounds!");
        }
    }

    private void initializeSongDetails(String path) {
        Logger.debug(getClass(), "initializeSongDetails path " + path);
        currentSongUri = path;

        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        Uri uri = Uri.parse(path);
        mediaMetadataRetriever.setDataSource(getApplicationContext(), uri);

        String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String composer = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String album = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);

        if (!isEmpty(title)) {
            songTitleLabel.setText(title);
        } else {
            songTitleLabel.setText(getSongName(path));
        }

        textViewAuthor.setText(composer);
        textViewAlbum.setText(album);
    }

    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = musicService.getDuration();
            long currentDuration = musicService.getCurrentPosition();

            if (utils == null) {
                utils = new SongUtilities();
            }
            // Displaying Total Duration time
            songTotalDurationLabel.setText(utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            songCurrentDurationLabel.setText(utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            songProgressBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
    }

    /**
     * When user starts moving the progress handler
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = musicService.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        musicService.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

    @Override
    public void onResume() {
        Logger.debug(getClass(), "onResume");
        super.onResume();

        if (SongsManager.getInstance().isNothingSelected() || SongsManager.isSongsObtained()) {
            safeClose(mProgressDialog);
        }

        if (musicService != null && musicService.isPlaying()) {
            Logger.debug(getClass(), "isPlaying true");
            updateSongUi();
        } else {
            Logger.debug(getClass(), "isPlaying false");
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String path = extras.getString(FileManagerActivity.FOLDER_PATH);
            getIntent().removeExtra(FileManagerActivity.FOLDER_PATH);
            Logger.debug(getClass(), "path is here! " + path);
            fetchSongsByPath(path);
        } else {
            handleIncomingContent(getIntent());
        }
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        Logger.debug(getClass(), "onDestroy unbind");
        mHandler.removeCallbacks(mUpdateTimeTask);
        mHandler = null;

        if (musicService != null) {
            musicService.stopMediaPlayer();
        }

        if (isBound) {
            unbindService(musicConnection);
            isBound = false;
        }
        musicService = null;
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String path = intent.getStringExtra(FileManagerActivity.FOLDER_PATH);
        Logger.debug(getClass(), "path is here now! " + path);
        Logger.debug(getClass(), "BroadcastReceiver maybe " + path);

        fetchSongsByPath(path);
        handleIncomingContent(intent);
    }

    private void fetchSongsByPath(String path) {
        if (!isEmpty(path) && path.equals("/")) {
            SongsManager.getInstance().fetchAllSongs(MainActivity.this, getApplicationContext());
        } else if (!isEmpty(path)) {
            SongsManager.getInstance().fetchSongsByFolder(MainActivity.this, getApplicationContext(), path);
        }
    }

    private void initializeMediaPlayer() {
        utils = new SongUtilities();

        // Listeners
        songProgressBar.setOnSeekBarChangeListener(this); // Important

        //todo is it necessary?
        // set Progress bar values
        songProgressBar.setProgress(0);
        songProgressBar.setMax(100);

        /**
         * Play button click event
         * plays a song and changes button to pause image
         * pauses a song and changes button to play image
         * */
        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check for already playing
                musicService.playResumeAction();
            }
        });

        /**
         * Stop button click event
         * stops a song
         * */
        btnStop.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                musicService.stopAction();
                btnPlay.setImageResource(R.drawable.play_action);

                // set Progress bar values
                songProgressBar.setProgress(0);
                songCurrentDurationLabel.setText(getResources().getString(R.string.songZeroTime));
            }
        });

        /**
         * Forward button click event
         * Forwards song specified seconds
         * */
        btnForward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // getInstance current song position
                int currentPosition = musicService.getCurrentPosition();
                // check if seekForward time is lesser than song duration
                if (currentPosition + seekForwardTime <= musicService.getDuration()) {
                    // forward song
                    musicService.seekTo(currentPosition + seekForwardTime);
                } else {
                    // forward to end position
                    musicService.seekTo(musicService.getDuration());
                }
            }
        });

        /**
         * Backward button click event
         * Backward song to specified seconds
         * */
        btnBackward.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // getInstance current song position

                int currentPosition = musicService.getCurrentPosition();
                // check if seekBackward time is greater than 0 sec
                if (currentPosition - seekBackwardTime >= 0) {
                    // forward song
                    musicService.seekTo(currentPosition - seekBackwardTime);
                } else {
                    // backward to starting position
                    musicService.seekTo(0);
                }

            }
        });

        /**
         * Next button click event
         * Plays next song by taking currentSongIndex + 1
         * */
        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check if next song is there or not
                musicService.nextAction();
            }
        });

        /**
         * Back button click event
         * Plays previous song by currentSongIndex - 1
         * */
        btnPrevious.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                musicService.previousAction();
            }
        });

        /**
         * Button Click event for Repeat button
         * Enables repeat flag to true
         * */
        btnRepeat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (musicService.isRepeat()) {
                    musicService.setIsRepeat(false);
                    Toast.makeText(getApplicationContext(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                    btnRepeat.setImageResource(R.drawable.btn_repeat);
                } else {
                    // make repeat to true
                    musicService.setIsRepeat(true);
                    Toast.makeText(getApplicationContext(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    musicService.setIsShuffle(false);
                    btnRepeat.setImageResource(R.drawable.btn_repeat_focused);
                    btnShuffle.setImageResource(R.drawable.btn_shuffle);
                }
            }
        });

        /**
         * Button Click event for Shuffle button
         * Enables shuffle flag to true
         * */
        btnShuffle.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (musicService.isShuffle()) {
                    musicService.setIsShuffle(false);
                    Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                    btnShuffle.setImageResource(R.drawable.btn_shuffle);
                } else {
                    // make repeat to true
                    musicService.setIsShuffle(true);
                    Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    musicService.setIsRepeat(false);
                    btnShuffle.setImageResource(R.drawable.btn_shuffle_focused);
                    btnRepeat.setImageResource(R.drawable.btn_repeat);
                }
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String newText) {
        Logger.debug(getClass(), "onQueryText Submit " + newText);
        searchSongs(newText);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Logger.debug(getClass(), "onQueryText Change " + newText);
        searchSongs(newText);
        return false;
    }

    private void searchSongs(String newText) {
        if (isEmpty(newText)) {
            songSearchList.clear();

            if (SongsManager.isAllPlaying()) {
                songSearchList.addAll(SongsManager.getAllSongList());
            } else {
                songSearchList.addAll(SongsManager.getSongFolderList());
            }

            adapter.notifyDataSetChanged();
            return;
        }

        newText = newText.toLowerCase();
        songSearchList.clear();
        adapter.notifyDataSetChanged();

        ArrayList<Song> songTempList = new ArrayList<>();
        if (SongsManager.isAllPlaying()) {
            songTempList.addAll(SongsManager.getAllSongList());
        } else {
            songTempList.addAll(SongsManager.getSongFolderList());
        }

        for (Song song : songTempList) {
            boolean isSongAdded = false;

            if (!isEmpty(song.getTitle())) {
                if (song.getTitle().toLowerCase().contains(newText)) {
                    songSearchList.add(song);
                    isSongAdded = true;
                }
            }

            if (!isEmpty(song.getComposer()) && !isSongAdded) {
                if (song.getComposer().toLowerCase().contains(newText)) {
                    Logger.debug(getClass(), "search song added");
                    songSearchList.add(song);
                    isSongAdded = true;
                }
                Logger.debug(getClass(), "search isEmpty song.getComposer");
            }

            if (!isEmpty(song.getAlbum()) && !isSongAdded) {
                if (song.getAlbum().toLowerCase().contains(newText)) {
                    songSearchList.add(song);
                    isSongAdded = true;
                }
                Logger.debug(getClass(), "search isEmpty song.getAlbum");
            }

            if (!isEmpty(song.getFileName()) && !isSongAdded) {
                if (song.getFileName().toLowerCase().contains(newText)) {
                    songSearchList.add(song);
                }
                Logger.debug(getClass(), "search isEmpty song.getFileName");
            }

        }

        //todo now songs not present in the new list, exclude it from the list

        Logger.debug(getClass(), "search finished");
        adapter.notifyDataSetChanged();
    }

}
