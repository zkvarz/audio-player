package com.zkvarz.audioplayer.services;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.core.structure.helpers.Logger;
import com.zkvarz.audioplayer.models.Song;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by kvarivoda on 10.12.2015.
 */
public class MusicService extends Service implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener {

    // Media Player
    private MediaPlayer mediaPlayer;
    private int currentSongIndex = 0;
    private static ArrayList<Song> songSearchList = new ArrayList<>();

    //shuffle flag and random
    private boolean isShuffle = false;
    private boolean isRepeat = false;

    private LocalBroadcastManager broadcaster;
    public static final String BROADCAST_UI_UPDATE = "com.zkvarz.audioplayer.services.MusicService.BROADCAST_UI_UPDATE";

    //binder
    private final IBinder musicBind = new MusicBinder();
    //notification id
    private static final int NOTIFY_ID = 1;

    public void onCreate() {
        //create the service
        super.onCreate();

        broadcaster = LocalBroadcastManager.getInstance(this);

        //initialize
        initMusicPlayer();
    }

    public void initMusicPlayer() {
        Logger.debug(getClass(), "initMusicPlayer");

        //create player
        mediaPlayer = new MediaPlayer();

        //set player properties
        mediaPlayer.setWakeMode(getApplicationContext(),
                PowerManager.PARTIAL_WAKE_LOCK);
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        //set listeners
        mediaPlayer.setOnPreparedListener(this);
        mediaPlayer.setOnCompletionListener(this);
        mediaPlayer.setOnErrorListener(this);
    }

    //pass song list
    public void setList(List<Song> theSongs) {
        songSearchList.clear();
        songSearchList.addAll(theSongs);
    }

    //binder
    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    //activity will bind to service
    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    //release resources when unbind
    @Override
    public boolean onUnbind(Intent intent) {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.stop(); //error
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                Logger.error(getClass(), e.toString());
            }
        }
        return false;
    }

    /**
     * Function to play a song
     *
     * @param songIndex - index of song
     */
    public void playSong(int songIndex) {


        Logger.debug(getClass(), "playSong " + songIndex);
        Logger.debug(getClass(), "playSong songSearchList size " + songSearchList.size());
        try {
            if (mediaPlayer != null) {
                mediaPlayer.reset();
            }

            initMusicPlayer();

            if (songSearchList.isEmpty()) {
                return;
            } else {
                Logger.debug(getClass(), "songSearchList size " + songSearchList.size());
            }

            mediaPlayer.setDataSource(songSearchList.get(songIndex).getPath());

            mediaPlayer.prepare();
            mediaPlayer.start();

            currentSongIndex = songIndex;

            setBroadcastUiUpdate();

        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    public void playResumeAction() {
        if (!songSearchList.isEmpty() && mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.pause();
            } else {
                // Resume song
                mediaPlayer.start();
            }
            setBroadcastUiUpdate();
        } else {
            Logger.error(getClass(), "media player == null");
            Logger.error(getClass(), "media player songSearchList size " + songSearchList.size());
            Logger.debug(getClass(), "media player: play at 0 position");
            playSong(0);
        }
    }

    public void stopAction() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.pause();
                mediaPlayer.seekTo(0);
            } catch (Exception e) {
                Logger.error(getClass(), e.toString());
            }
            setBroadcastUiUpdate();
        } else {
            Logger.error(getClass(), "media player == null");
        }
    }

    public void nextAction() {
        if (!songSearchList.isEmpty() && (currentSongIndex < (songSearchList.size() - 1))) {
            currentSongIndex = currentSongIndex + 1;
            playSong(currentSongIndex);
        } else {
            // play first song
            playSong(0);
            currentSongIndex = 0;
        }
    }

    public void previousAction() {
        if (!songSearchList.isEmpty() && currentSongIndex > 0) {
            currentSongIndex = currentSongIndex - 1;
            playSong(currentSongIndex);
        } else {
            // play last song
            currentSongIndex = songSearchList.size() - 1;
            playSong(currentSongIndex);
        }
    }

    public void stopMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public boolean isMediaPlayerExist() {
        return mediaPlayer != null;
    }

    /**
     * On Song Playing completed
     * if repeat is ON play same song again
     * if shuffle is ON play random song
     */
    @Override
    public void onCompletion(MediaPlayer arg0) {

        // check for repeat is ON or OFF
        if (isRepeat) {
            // repeat is on play same song again
            playSong(currentSongIndex);
        } else if (isShuffle) {
            // shuffle is on - play a random song
            if (!songSearchList.isEmpty()) {
                Random rand = new Random();
                currentSongIndex = rand.nextInt((songSearchList.size() - 1) + 1);
                playSong(currentSongIndex);
            }
        } else {
            // no repeat or shuffle ON - play next song
            if (!songSearchList.isEmpty() && (currentSongIndex < (songSearchList.size() - 1))) {
                currentSongIndex = currentSongIndex + 1;
                playSong(currentSongIndex);
            } else {
                // play first song
                currentSongIndex = 0;
                if (!songSearchList.isEmpty()) {
                    playSong(0);
                }
            }
        }
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Logger.error(getClass(), "MUSIC PLAYER Playback Error");
        mp.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        //start playback
        mp.start();

        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(getApplicationContext());
        mNotificationBuilder.setOngoing(true);
        mNotificationBuilder.setAutoCancel(false);

        //Build the notification object and set its flags.
        Notification notification = mNotificationBuilder.build();
        notification.flags = Notification.FLAG_FOREGROUND_SERVICE |
                Notification.FLAG_NO_CLEAR |
                Notification.FLAG_ONGOING_EVENT;

        startForeground(NOTIFY_ID, notification);
    }

    //playback methods
    public int getCurrentPosition() {
        int pos = 0;
        if (mediaPlayer != null) {
            try {
                pos = mediaPlayer.getCurrentPosition();
            } catch (Exception e) {
                Logger.error(getClass(), e.toString());
            }
        }
        return pos;
    }

    public int getDuration() {
        int duration = 0;
        if (mediaPlayer != null) {
            try {
                duration = mediaPlayer.getDuration();
            } catch (Exception e) {
                Logger.error(getClass(), e.toString());
            }
        }
        return duration;
    }

    public boolean isPlaying() {
        boolean isPlaying = false;
        if (mediaPlayer != null) {
            try {
                isPlaying = mediaPlayer.isPlaying();
            } catch (Exception e) {
                Logger.error(getClass(), e.toString());
            }
        }
        return isPlaying;
    }

    public void seekTo(int posn) {
        if (mediaPlayer != null) {
            mediaPlayer.seekTo(posn);
        } else {
            Logger.error(getClass(), "seekTo media player == null");
        }
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
    }

    public void setBroadcastUiUpdate() {
        Intent intent = new Intent(BROADCAST_UI_UPDATE);
        broadcaster.sendBroadcast(intent);
    }

    public int getCurrentSongIndex() {
        return currentSongIndex;
    }

    public void setCurrentSongIndex(int currentSongIndex) {
        this.currentSongIndex = currentSongIndex;
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public void setIsShuffle(boolean isShuffle) {
        this.isShuffle = isShuffle;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public void setIsRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }
}
