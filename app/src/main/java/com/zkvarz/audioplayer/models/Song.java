package com.zkvarz.audioplayer.models;

import static android.text.TextUtils.isEmpty;
import static com.zkvarz.audioplayer.utilities.SongUtilities.getSongName;

/**
 * Created by kvarivoda on 09.10.2015.
 */
public class Song {
    private String fileName = "";
    private String path = "";

    private String title = "";
    private String composer = "";
    private String album = "";
    private long duration;

    public Song(String fileName, String path) {
        if (!isEmpty(fileName)) {
            this.title = fileName;
        } else {
            this.title = getSongName(path);
        }
        this.fileName = fileName;
        this.path = path;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title != null) {
            this.title = title;
        }
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        if (composer != null) {
            this.composer = composer;
        }
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        if (album != null) {
            this.album = album;
        }
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof Song) {
            Song song = (Song) o;

            return fileName != null && fileName.equals(song.getFileName()) &&
                    path != null && path.equals(song.getPath()) &&
                    title != null && title.equals(song.getTitle()) &&
                    composer != null && composer.equals(song.getComposer()) &&
                    album != null && album.equals(song.getAlbum()) &&
                    (duration == song.getDuration());
        } else {
            return super.equals(o);
        }
    }

}
