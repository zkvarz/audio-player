package com.zkvarz.audioplayer.manager;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.core.structure.helpers.Logger;
import com.zkvarz.audioplayer.models.Song;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static android.text.TextUtils.isEmpty;
import static com.zkvarz.audioplayer.utilities.SongUtilities.isMusicFile;

/**
 * Created by kvarivoda on 10.09.2015.
 */
public class SongsManager {

    final String extStore = System.getenv("EXTERNAL_STORAGE");
    final String secStore = System.getenv("SECONDARY_STORAGE");

    public static final int BY_AUTHOR = 0;
    public static final int BY_ALBUM = 1;

    private static boolean isAllPlaying;

    private static SongsManager songsManager;

    private static List<Song> songAllList = new ArrayList<>();
    private static boolean isAllSongsObtained = false;
    private static boolean isSongDetailsObtained = false;

    private static ArrayList<Song> songFolderList = new ArrayList<>();
    private static boolean isFolderSongsObtained = false;
    private static boolean isFolderSongDetailsObtained = false;

    private boolean isNothingSelected = true;

    private String folderPath;

    private Context context;
    private SongFetcher songFetcher;
    private SongDetailsFetcher songDetailsFetcher;
    private OnSongsFetchedListener onSongsFetchedListener;

    public interface OnSongsFetchedListener {
        void onSongsFetched(List<Song> songList);

        void onSongsDetailsFetched(List<Song> songList);
    }

    // singleton pattern for creating an object
    public static SongsManager getInstance() {
        if (songsManager == null) {
            songsManager = new SongsManager();
        }
        return songsManager;
    }

    public void fetchAllSongs(OnSongsFetchedListener onSongsFetchedListener, Context context) {
        isAllPlaying = true;
        isNothingSelected = false;

        if (!isSongDetailsObtained) {
            this.onSongsFetchedListener = onSongsFetchedListener;
            this.context = context;

            if (songFetcher != null) {
                songFetcher.cancel(true);
            }
            songFetcher = new SongFetcher();
            songFetcher.execute();
        } else {
            onSongsFetchedListener.onSongsFetched(songAllList);
        }
    }

    public void fetchSongsByFolder(OnSongsFetchedListener onSongsFetchedListener, Context context, String folder) {
        isAllPlaying = false;
        isNothingSelected = false;

        isFolderSongsObtained = false;
        isFolderSongDetailsObtained = false;

        if (songFetcher != null) {
            songFetcher.cancel(true);
        }

        songFolderList.clear();

        this.onSongsFetchedListener = onSongsFetchedListener;
        this.context = context;

        songFetcher = new SongFetcher();
        songFetcher.execute();
        this.folderPath = folder;
    }

    /**
     * Function to read all mp3 files from directory
     */
    public void fetchMusicFiles(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (File aListFile : listFile) {

                if (aListFile.isDirectory()) {
                    fetchMusicFiles(aListFile);
                } else {
                    if (isMusicFile(aListFile)) {
                        addFileToList(aListFile);
                    }
                }
            }
        }
    }

    private void addFileToList(File file) {
        String songFileName = file.getName();
        // Remove all characters after "."
        songFileName = songFileName.split("\\.", 2)[0];
        String songPath = file.getPath();

        Song songObj = new Song(songFileName, songPath);

        if (isAllPlaying) {
            songAllList.add(songObj);
        } else {
            songFolderList.add(songObj);
        }

    }

    private void fetchSongDetails() {
        Logger.debug(getClass(), "fetchSongDetails()");
        ArrayList<Song> songList = new ArrayList<>();

        if (isAllPlaying) {
            songList.addAll(songAllList);
        } else {
            songList.addAll(songFolderList);
        }

        for (Song song : songList) {
            String songPath = song.getPath();


            if (context != null) {
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                Uri uri = Uri.parse(songPath);
                mediaMetadataRetriever.setDataSource(context, uri);

                String title = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                String composer = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                String album = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                long duration = Long.valueOf(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));

                if (!TextUtils.isEmpty(title)) {
                    song.setTitle(title);
                }

                if (!TextUtils.isEmpty(composer)) {
                    song.setComposer(composer);
                }

                if (!TextUtils.isEmpty(album)) {
                    song.setAlbum(album);
                }

                song.setDuration(duration);

                Logger.debug(getClass(), "real data title is " + title);
                Logger.debug(getClass(), "real data author is " + composer);
                Logger.debug(getClass(), "real data album is " + album);
                Logger.debug(getClass(), "real data duration is " + duration);
//            Logger.debug(getClass(), "real data duration converted is " + new SongUtilities().milliSecondsToTimer(duration));
                Logger.debug(getClass(), "real data duration songPath is " + songPath);

            }
        }
        Logger.debug(getClass(), "isSongDetailsObtained = true!");
    }

    private class SongFetcher extends AsyncTask<Void, Void, List<Song>> {

        @Override
        protected List<Song> doInBackground(Void... params) {

            if (isAllPlaying) {
                // Fetch all files from external storage
                File externalRoot = new File(extStore);
                fetchMusicFiles(externalRoot);

                // Fetch all files from secondary SD storage
                if (getRemovableStorage() != null) {
                    File secondaryRoot = new File(secStore);
                    fetchMusicFiles(secondaryRoot);
                }

                Logger.debug(getClass(), "songList.size() ! " + songAllList.size());
            } else {
                if (folderPath != null) {
                    if (isMusicFile(folderPath)) {
                        addFileToList(new File(folderPath));
                    } else {
                        File folderRoot = new File(folderPath);
                        fetchMusicFiles(folderRoot);
                    }

                }
                Logger.debug(getClass(), "songFolderList.size() ! " + songAllList.size());
            }

            return songAllList;
        }

        @Override
        protected void onPostExecute(List<Song> result) {
            super.onPostExecute(result);

            if (isAllPlaying) {
                isAllSongsObtained = true;
                if (onSongsFetchedListener != null)
                    onSongsFetchedListener.onSongsFetched(songAllList);
            } else {
                isFolderSongsObtained = true;
                if (onSongsFetchedListener != null)
                    onSongsFetchedListener.onSongsFetched(songFolderList);
            }

            if (songDetailsFetcher != null) {
                songDetailsFetcher.cancel(true);
            }
            songDetailsFetcher = new SongDetailsFetcher();
            songDetailsFetcher.execute();
        }
    }


    private class SongDetailsFetcher extends AsyncTask<Void, Void, List<Song>> {

        @Override
        protected List<Song> doInBackground(Void... params) {
            fetchSongDetails();
            return songAllList;
        }

        @Override
        protected void onPostExecute(List<Song> result) {
            super.onPostExecute(result);
            if (isAllPlaying) {
                isSongDetailsObtained = true;
                if (onSongsFetchedListener != null)
                    onSongsFetchedListener.onSongsDetailsFetched(songAllList);
            } else {
                isFolderSongDetailsObtained = true;
                if (onSongsFetchedListener != null)
                    onSongsFetchedListener.onSongsDetailsFetched(songFolderList);
            }

        }
    }

    /**
     * Uses the Environmental variable "SECONDARY_STORAGE" to locate a removable micro sdcard
     *
     * @return the primary secondary storage directory or
     * {@code null} if there is no removable storage
     */
    public File getRemovableStorage() {
        final String value = System.getenv("SECONDARY_STORAGE");
        if (!isEmpty(value)) {
            final String[] paths = value.split(":");
            for (String path : paths) {
                Logger.debug(getClass(), "path! " + path);
                File file = new File(path);
                if (file.isDirectory()) {
                    return file;
                }
            }
        }
        return null;
    }

    public void sortByTitle(OnSongsFetchedListener onSongsFetchedListener, Context context) {
        if (onSongsFetchedListener == null || context == null) {
            this.onSongsFetchedListener = onSongsFetchedListener;
            this.context = context;
            if (onSongsFetchedListener == null) {
                Logger.debug(getClass(), "onSongsFetchedListener == null");
            }
            if (context == null) {
                Logger.debug(getClass(), "context == null");
            }
        }

        if (isAllPlaying) {
            Logger.debug(getClass(), "songAllList size " + songAllList.size());
            Collections.sort(songAllList, new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2) {
                    return song1.getTitle().compareTo(song2.getTitle());
                }
            });
            Logger.debug(getClass(), "print all titles");
            for (Song song : getAllSongList()) {
                Logger.debug(getClass(), "SONG title " + song.getTitle());
                Logger.debug(getClass(), "SONG  name " + song.getFileName());
            }
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songAllList);
        } else {
            Logger.debug(getClass(), "songFolderList size " + songFolderList.size());
            Collections.sort(songFolderList, new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2) {
                    return song1.getTitle().compareTo(song2.getTitle());
                }
            });
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songFolderList);
        }
    }

    public void sortByDuration(OnSongsFetchedListener onSongsFetchedListener, Context context) {
        if (onSongsFetchedListener == null || context == null) {
            this.onSongsFetchedListener = onSongsFetchedListener;
            this.context = context;
        }

        ArrayList<Song> songTempList = new ArrayList<>();
        if (isAllPlaying) {
            songTempList.addAll(songAllList);
            Collections.sort(songTempList, new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2) {
                    return Long.valueOf(song1.getDuration()).compareTo(song2.getDuration());
                }
            });
            songAllList.clear();
            songAllList.addAll(songTempList);
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songAllList);
        } else {
            songTempList.addAll(songFolderList);
            Collections.sort(songTempList, new Comparator<Song>() {
                @Override
                public int compare(Song song1, Song song2) {
                    return Long.valueOf(song1.getDuration()).compareTo(song2.getDuration());
                }
            });
            songFolderList.clear();
            songFolderList.addAll(songTempList);
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songFolderList);
        }
    }

    public void sortByParam(OnSongsFetchedListener onSongsFetchedListener, Context context, int param) {
        if (onSongsFetchedListener == null || context == null) {
            this.onSongsFetchedListener = onSongsFetchedListener;
            this.context = context;
        }

        ArrayList<Song> songTempList = new ArrayList<>();

        if (isAllPlaying) {
            songTempList.addAll(getSongListByKey(songAllList, param));
            songAllList.clear();
            songAllList.addAll(songTempList);
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songAllList);
        } else {
            songTempList.addAll(getSongListByKey(songFolderList, param));
            songFolderList.clear();
            songFolderList.addAll(songTempList);
            if (onSongsFetchedListener != null)
                onSongsFetchedListener.onSongsFetched(songFolderList);
        }
    }

    private ArrayList<Song> getSongListByKey(List<Song> songTempList, int param) {

        TreeMap<String, ArrayList<Song>> songHashMap = new TreeMap<>();
        ArrayList<Song> sortedSongList = new ArrayList<>();

        for (Song song : songTempList) {
            //check param
            String key;
            if (param == BY_AUTHOR) {
                key = song.getComposer();
            } else {
                key = song.getAlbum();
            }

            if (key == null) {
                return sortedSongList;
            }

            if (songHashMap.containsKey(key)) {
                ArrayList<Song> songsByKey = songHashMap.get(key);
                if (!songsByKey.contains(song))
                    songsByKey.add(song);
            } else {
                ArrayList<Song> songsByKey = new ArrayList<>();
                songsByKey.add(song);
                songHashMap.put(key, songsByKey);
            }
        }

        for (Map.Entry<String, ArrayList<Song>> entry : songHashMap.entrySet()) {

            ArrayList<Song> value = entry.getValue();
            for (Song song : value) {
                sortedSongList.add(song);
            }
        }

        return sortedSongList;
    }

    public static boolean isSongsObtained() {
        if (isAllPlaying) {
            return isAllSongsObtained;
        } else {
            return isFolderSongsObtained;
        }
    }

    public static boolean isSongDetailsObtained() {
        if (isAllPlaying) {
            return isSongDetailsObtained;
        } else {
            return isFolderSongDetailsObtained;
        }
    }

    public boolean isNothingSelected() {
        return isNothingSelected;
    }

    public static List<Song> getAllSongList() {
        return songAllList;
    }

    public static ArrayList<Song> getSongFolderList() {
        return songFolderList;
    }

    public static void setIsAllPlaying(boolean isAllPlaying) {
        SongsManager.isAllPlaying = isAllPlaying;
    }

    public static boolean isAllPlaying() {
        return isAllPlaying;
    }
}
