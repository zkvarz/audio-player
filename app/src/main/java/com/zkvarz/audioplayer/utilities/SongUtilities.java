package com.zkvarz.audioplayer.utilities;

import java.io.File;

import static android.text.TextUtils.isEmpty;

/**
 * Created by kvarivoda on 10.09.2015.
 */
public class SongUtilities {

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString;

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Function to getInstance Progress percentage
     *
     * @param currentDuration -
     * @param totalDuration - total duration of the composition
     */
    public int getProgressPercentage(long currentDuration, long totalDuration) {
        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        Double percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to change progress to timer
     *
     * @param progress      -
     * @param totalDuration returns current duration in milliseconds
     */
    public int progressToTimer(int progress, int totalDuration) {
        totalDuration = (totalDuration / 1000);
        int currentDuration = (int) ((((double) progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    /**
     * Check if file has audio extension
     */
    public static boolean isMusicFile(File file) {
        String fileName = file.getName();
        return (isMusicFile(fileName));
    }

    /**
     * Check if file has proper extension by string
     */
    public static boolean isMusicFile(String file) {
        return !isEmpty(file) && (file.endsWith(".mp3") || file.endsWith(".MP3")
                || file.endsWith(".Mp3") || file.endsWith(".mP3") || file.endsWith(".flac")
                || file.endsWith(".ogg") || file.endsWith(".mid") || file.endsWith(".xmf")
                || file.endsWith(".mxmf") || file.endsWith(".imy") || file.endsWith(".mkv")
                || file.endsWith(".rtttl") || file.endsWith(".rtx") || file.endsWith(".ota"));
    }

    /**
     * Get name of the file by its path
     */
    public static String getSongName(String path) {
        File file = new File(path);
        String songFileName = file.getName();
        // Remove all characters after "."
        songFileName = songFileName.split("\\.", 2)[0];
        return songFileName;
    }

}
