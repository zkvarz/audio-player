# README #

### Create an application: media player. ###

* Minimal version: Android 4.0.
* Supported resolutions are mdpi, hdpi, xhdpi, xxhdpi.
* Tablet support may be as an addition.

### 1st Screen. Playlist. ###
1. Play all songs on the device
2. Play folder

### Lists consists of: ###
1. Icon (note or something similar), while playing changes to icon "play".
2. Name of the songs
3. Name of Author.
4. Name of Album
5. Duration of the song.

In action bar ability to search by typing:
Search by filter: name of the composition, Author, Album.
(filter is case insensitive)

Ability to sort list by: name, author, album, duration.

Below on this screen panel with the player that consists of:
previous, play/pause, stop, next button.

Progress line, while moving the line music plays from this period of time.
Information how long composition is playing.
Information about duration of the composition.
Name of the composition.
Author
Album.
In addition: choice to play tracks in random order or by circles.

### 2nd Screen. File browser. ###
Needed for choosing the folder that will be played. Shows directories and only musical files in them.
After choosing the folder we go back to the main screen with mode of displaying list of compositions from the folder.

Important:

1. All the buttons in the app should be clickable and visually responding on the click.
2. App is able to play musical files in active or in the hidden state.
3. Also app registers in the system as a program that works with the audio files. So after opening a musical file app will appear on the screen as one of the options to play the file, after choosing the option file plays.

While creating the program use all possible features of the android platform.